<?php

namespace ProjetAnimalerie\Controller;

class ControllerTools
{
    public static function upload_file($name)
    {
        // $_FILES[$name] => <input type="file" name="..." >

        $check = true;
        $target_file = null;

        if (!isset($_FILES[$name]))
        {
            $check = false;
            $message = "Fichier trop volumineux";
        }
        else if($_FILES[$name]['error'] != 0)
        {
            $check = false;
            switch($_FILES[$name]['error'])
            {
                case UPLOAD_ERR_INI_SIZE:
                    $message = "Fichier trop volumineux";
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                    $message = "Fichier trop volumineux";
                    break;
                case UPLOAD_ERR_PARTIAL:
                    $message = "Le fichier n'a été que partiellement téléchargé.";
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $check = true;
                    $message = "Aucun fichier n'a été téléchargé.";
                    break;
                case UPLOAD_ERR_NO_FILE:
                case UPLOAD_ERR_CANT_WRITE:
                case UPLOAD_ERR_EXTENSION:
                    $message = "Erreur interne PHP.";
                    break;
            }
        }

        if ($check && !empty($_FILES[$name]["name"]))
        {
            // Un fichier a été envoyé
            // Définition du dossier de téléchargement
            $target_dir = "src/View/images/";

            // $_FILES[$name] correspond au fichier de <input type="file" name="image">
            // tmp_name emplacement provisoire du fichier

            // Contrôle du fichier
            $mime = mime_content_type($_FILES[$name]["tmp_name"]);

            switch ($mime)
            {
                case "image/jpeg":
                case "image/png":
                    break;
                default:
                    // Erreur de MIME
                    $check = false;
                    $message = "Type de fichier non pris en charge.";
                    break;
            }

            if ($check)
            {
                // Contrôle de la taille de l'image

                $max_upload = (int)substr(ini_get('upload_max_filesize'),0,-1);
                $max_post = (int)substr(ini_get('post_max_size'),0,-1);
                $memory_limit = (int)substr(ini_get('memory_limit'),0,-1);
                $upload_mb = min($max_upload, $max_post, $memory_limit);
                $max_file_size = $upload_mb * 1024 * 1024;

                if ($_FILES[$name]["size"] > $max_file_size)
                {
                    // Erreur taille de fichier
                    $check = false;
                    $message = "La taille du fichier ne peut pas dépasser $upload_mb Mo.";
                }
                else
                {
                    // Tous les tests sont passés (tout va bien)
                    // Préparation du nom du fichier
                    // Empreinte SHA1
                    $sha1 = sha1_file($_FILES[$name]["tmp_name"]);
                    $fingerprint = substr($sha1, 0, 7);
                    // Horodatation
                    $datetime = date("Ymd_His");
                    // Extension
                    $extension = strtolower(pathinfo($_FILES[$name]["name"],PATHINFO_EXTENSION));
                    // Construction du nom du fichier
                    $target_file = "img_" . $fingerprint . "_" . $datetime . "." . $extension;

                    // Déplacement du fichier
                    if (move_uploaded_file($_FILES[$name]["tmp_name"], $target_dir . $target_file))
                    {
                        // Transfert Terminé
                    }
                    else
                    {
                        // Erreur de transfert de fichier
                        $check = false;
                        $message = "Une erreur est survenue pendant le transfert du fichier.";
                    }
                }
            }
        }

        if (!$check)
        {
            include "src/View/templates/erreur.html";
        }

        $result = [
            "succeed" => $check,
            "filename" => $target_file
        ];

        return $result;
    }
}