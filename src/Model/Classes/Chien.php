<?php
namespace ProjetAnimalerie\Model\Classes;

use \InvalidArgumentException;
use ProjetAnimalerie\Model\Dao\ChienDao;

class Chien extends Animal
{
    private $id_animal;
    private $race;

    protected function get_id_animal() {return $this->id_animal; }
    protected function set_id_animal($value) {$this->id_animal = $value; }

    protected function get_race() {return $this->race; }
    protected function set_race($value) {$this->race = $value; }

    public function __construct()
    {
        $nb = func_num_args();
        switch ($nb)
        {
            case 0:
                $this->construct_0();
                break;
            case 7:
                $id = func_get_arg(0);
                $nom = func_get_arg(1);
                $sexe = func_get_arg(2);
                $naissance = func_get_arg(3);
                $image = func_get_arg(4);
                $id_animal = func_get_arg(5);
                $race = func_get_arg(6);
                $this->construct_7($id, $nom, $sexe, $naissance, $image, $id_animal, $race);
                break;
            default:
                throw new InvalidArgumentException("Invalid parameters number");
                break;
        }
    }

    private function construct_0()
    {
        // valeurs par défaut

        // Constructeur de Animal
        parent::__construct();

        // Propriétés de Chien
        $this->id_animal = null;
        $this->race = "";
    }

    private function construct_7($id, $nom, $sexe, $naissance, $image, $id_animal, $race)
    {
        // Constructeur de Animal
        parent::__construct($id, $nom, $sexe, $naissance, $image);

        // valeurs fournies en paramètres
        $this->id_animal = $id;
        $this->race = $race;
    }

    public function __toString()
    {
        return $this->nom;
    }

    public static function getAll()
    {
        $dao = new ChienDao();
        return $dao->getAll();
    }

    public static function get($id)
    {
        $dao = new ChienDao();
        return $dao->get($id);
    }
}