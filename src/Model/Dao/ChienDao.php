<?php

namespace ProjetAnimalerie\Model\Dao;

use ProjetAnimalerie\Model\Dal\Dal;
use \PDO;

class ChienDao extends Dal
{
    private $classname = "ProjetAnimalerie\\Model\\Classes\\Chien";
    private $table = "chien";
    private $structure = ['id', 'nom', 'sexe', 'naissance', 'image','id_animal', 'race'];

    public function getAll()
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  INNER JOIN `animal` ON `chien`.`id_animal` = `animal`.`id`;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function get($id)
    {
        $query = "SELECT *
                  FROM `{$this->table}`
                  INNER JOIN `animal` ON `chien`.`id_animal` = `animal`.`id`
                  WHERE `id` = :id;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $object = $stmt->fetch();
        $this->Close();
        return $object;
    }
}