<?php

namespace ProjetAnimalerie\Model\Dao;

use ProjetAnimalerie\Model\Dal\Dal;
use \PDO;

class AnimalDao extends Dal
{
    private $classname = "ProjetAnimalerie\\Model\\Classes\\Animal";
    private $table = "animal";
    private $structure = ['id', 'nom', 'sexe', 'naissance', 'image'];

    public function getAll()
    {
        $query = "SELECT * 
                  FROM `{$this->table}`;
                  ";
        $this->Open();
        $stmt = $this->dbh->prepare($query);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classname, $this->structure);
        $rows = $stmt->fetchAll();
        $this->Close();
        return $rows;
    }

    public function insert($nom, $sexe, $naissance, $image)
    {
        global $debugMode;

        $query = "INSERT INTO `{$this->table}`
                  (`nom`, `sexe`, `naissance`, `image`)
                  VALUES
                  (:nom, :sexe, :naissance, :image);
                  ";

        $this->Open();
        $stmt = $this->dbh->prepare($query);

        $stmt->bindParam(":nom", $nom, PDO::PARAM_STR);
        $stmt->bindParam(":sexe", $sexe, PDO::PARAM_STR);
        $stmt->bindParam(":naissance", $naissance, PDO::PARAM_STR);
        $stmt->bindParam(":image", $image, PDO::PARAM_STR);

        $nbRows = $stmt->execute();
        if ($nbRows == 1)
        {
            $id = $this->dbh->lastInsertId();
        }
        else
        {
            $id = 0;
            if ($debugMode)
            {
                echo '<div class="alert alert-danger" role="alert">' . "\n";
                echo $stmt->errorInfo()[2];
                echo '</div>' . "\n";
            }
        }
        $this->Close();
        return $id;
    }

    /**
     * Mise à jour d'un animal
     * @param int $id identifiant
     * @param string $nom nom
     * @param string $sexe Mâle ou Femelle
     * @param string $naissance YYYY-MM-DD
     * @param string $image nom du fichier image
     */
    public function update ($id, $nom, $sexe, $naissance, $image)
    {
        global $debugMode;

        if (empty($image))
        {
            $query = "UPDATE `{$this->table}`
                        SET
                            `nom` = :nom,
                            `sexe` = :sexe,
                            `naissance` = :naissance
                        WHERE `id` = :id; 
                    ";
        }
        else
        {
            $query = "UPDATE `{$this->table}`
                        SET
                            `nom` = :nom,
                            `sexe` = :sexe,
                            `naissance` = :naissance,
                            `image` = :image
                        WHERE `id` = :id; 
                    ";
        }

        $this->Open();
        $stmt = $this->dbh->prepare($query);

        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->bindParam(":nom", $nom, PDO::PARAM_STR);
        $stmt->bindParam(":sexe", $sexe, PDO::PARAM_STR);
        $stmt->bindParam(":naissance", $naissance, PDO::PARAM_STR);
        if (!empty($image))
        {
            $stmt->bindParam(":image", $image, PDO::PARAM_STR);        
        }

        $nbRows = $stmt->execute();

        if ($debugMode && $nbRows != 1)
        {
            echo '<div class="alert alert-danger" role="alert">' . "\n";
            echo $stmt->errorInfo()[2];
            echo '</div>' . "\n";
        }

        $this->Close();
        return $nbRows;
    }

    /**
     * Suppression d'un animal
     * @param int $id identifiant de l'animal
     */
    public function delete ($id)
    {
        global $debugMode;

        $query = "DELETE FROM `{$this->table}`
                  WHERE `id` = :id; 
                ";

        $this->Open();
        $stmt = $this->dbh->prepare($query);

        $stmt->bindParam(":id", $id, PDO::PARAM_INT);

        $nbRows = $stmt->execute();

        if ($debugMode && $nbRows != 1)
        {
            echo '<div class="alert alert-danger" role="alert">' . "\n";
            echo $stmt->errorInfo()[2];
            echo '</div>' . "\n";
        }

        $this->Close();
        return $nbRows;
    }
}